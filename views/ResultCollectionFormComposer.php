<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Grading System Using OOP</title>
    <link rel="stylesheet" href="style.css">
</head>
    <body>
        <form action="processComposer.php" method="post" >
            <fieldset>
                <legend>Student's Information</legend>
                Name of Student:
                <input type="text" name="name"><br>
                Student ID:
                <input type="number" name="studentId">
            </fieldset>
            <fieldset>
                <legend>Enter Result</legend>

                Bangla:
                <input type="number" name="bangla"><br>
                English:
                <input type="number" name="english"><br>
                Math:
                <input type="number" name="math"><br>
                <input type="submit" value="Add Result" name="addResult">
                <input type="submit" value="Show Result" name="showResult">
            </fieldset>
        </form>
    </body>
</html>